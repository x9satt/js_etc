function SEA(){

	var randloc = Math.floor(Math.random()*5);
	var loc1 = randloc;
	var loc2 = randloc+1;
	var loc3 = randloc+2;

	var guess;
	var hits = 0;
	var guesses = 0;

	var isSunk = false;

	while (isSunk==false){
		guess = prompt("Ready, aim, fire! (enter a number from 0 to 6):");
		if (guess < 0 || guess > 6){
			alert("Please enter a valid number!");
		}
		else{
			guesses += 1;
			if (guess == loc1 || guess == loc2 || guess==loc3){
				hits += 1;
				alert("HIT!");
				if (hits==3){
					isSunk=true;
					alert("You sank my ship!");
				}
			}
			else{
				alert("MISS!");
			}
		}
	}

	alert("You took " + guesses + " guesses to sink the ship, which means your shooting accuracy was " + (3/guesses)*100 + "%");
	
}