canvas = null;
ctx = null;

t1 = parseInt(prompt('Задайте время работы красного сигнала светофора(в секундах): '))*1000;
t2 = 1000;
t3 = parseInt(prompt('Задайте время работы зелёного сигнала светофора(в секундах): '))*1000;

function init(){
	canvas = document.getElementById("light");
	ctx = canvas.getContext("2d");

	var ta = (t1+t2+t3+t2);
	
	render();
	setInterval(render, ta);
	
}

function render(){
	// красный
	ctx.beginPath();
	ctx.fillStyle = "rgb(255,0,0)";
	ctx.arc( 23, 21, 20, 0, 2*Math.PI );
	ctx.fill();
	// черные 2,3
	ctx.beginPath();
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.arc( 23, 71, 20, 0, 2*Math.PI );
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.arc( 23, 121, 20, 0, 2*Math.PI );
	ctx.fill();

	setTimeout(function(){
		// чёрный вместо красного
		ctx.beginPath();
		ctx.fillStyle = "rgb(0,0,0)";
		ctx.arc( 23, 21, 20, 0, 2*Math.PI );
		ctx.fill();
		// жёлтый 
		ctx.beginPath();
		ctx.fillStyle = "rgb(255,255,0)";
		ctx.arc( 23, 71, 20, 0, 2*Math.PI );
		ctx.fill();
		setTimeout(function(){
			// чёрный вместо жёлтого
			ctx.beginPath();
			ctx.fillStyle = "rgb(0,0,0)";
			ctx.arc( 23, 71, 20, 0, 2*Math.PI );
			ctx.fill();
			// зелёный
			ctx.beginPath();
			ctx.fillStyle = "rgb(0,255,0)";
			ctx.arc( 23, 121, 20, 0, 2*Math.PI );
			ctx.fill();	
			setTimeout(function(){
				// чёрный вместо зелёного
				ctx.beginPath();
				ctx.fillStyle = "rgb(0,0,0)";
				ctx.arc( 23, 121, 20, 0, 2*Math.PI );
				ctx.fill();
				// жёлтый 
				ctx.beginPath();
				ctx.fillStyle = "rgb(255,255,0)";
				ctx.arc( 23, 71, 20, 0, 2*Math.PI );
				ctx.fill();
			},t3);
		},t2);
	},t1);
	
	


	



	

}

